// by TStudios
// Settings to apply everywhere

// Per-style settings (More advanced)
config.style = {} // Ignore this lol
config.style.extras = {
  color: "#ff0000" // extras color
}
config.style.light = {
  color: "#222222", // Default: #000000
  bgcolor: "#ffffff" // Default: #ffffff
}
config.style.dark = {
  color: "#ffffff", // Default: #ffffff
  bgcolor: "#222222" // Default: #222222
  // bgcolor: "#0F0F0F" // left as an example that u can customise this a lot :D
}
