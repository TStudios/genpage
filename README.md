# Documentation
None yet, please read the files (especially [template/index.html](https://gitlab.com/TStudios/genpage/blob/master/template/index.html), [genpage.js](https://gitlab.com/TStudios/genpage/blob/master/genpage.js), [globalsettings.js](https://gitlab.com/TStudios/genpage/blob/master/globalsettings.js) and maybe also [template/file2.html](https://gitlab.com/TStudios/genpage/blob/master/template/file2.html)) and maybe write some Documentation in a Pull Request?
# Starting
To start using this, you *must* have [genpage.js](https://gitlab.com/TStudios/genpage/blob/master/genpage.js) (if you use the templates, in the root of your server's web-directory), and it is **strongly** enouraged to use the ***templates***, and then modify them.
You **must** have **all** of the config values specified in [template/index.html](https://gitlab.com/TStudios/genpage/blob/master/template/index.html) under "config" and **all** of the ones specified in [globalsettings.js](https://gitlab.com/TStudios/genpage/blob/master/globalsettings.js)
# Copyright
Copyright (c) 2019 Biocloud Productions. All Rights Reserved.
Biocloud Productions and the Biocloud Productions logo are Copyright (c) 2017-2019 TStudios. All Rights Reserved.
