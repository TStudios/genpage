// by TStudios
// ADVANCED Code - Generator etc...

function openmenu(menunum) {
  console.log(popupguis[menunum])
}
function title() {
  document.getElementById('TITLE_DAMN_IT').innerHTML=config.title;
  document.getElementById('InPageTitle').innerHTML="<span class='oof'>►</span> " + config.inpagetitle + " <span class='oof'>◄</span>";
}
function setupcolors() {
  oof=document.getElementsByClassName('oof')
  for (var i = 0; i < oof.length; i++) {
    oof[i].style.color=config.style.extras.color
  }
}
function setCookie(cname, cvalue) {
  var expire = new Date();
  expire.setTime(expire.getTime() + 1 * 3600 * 1000 * 24 * 90); // (1 hour * 24 = 1 day) * 90 = 90 days
  document.cookie = cname + "=" + cvalue + ";expires="+expire.toUTCString()+";path=/";
}
function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return null;
}
function theme(load) {
  if(load) {
    if(getCookie("theme") != null) {
      config.theme=getCookie("theme")
    }
  }
  if(config.theme=="dark"){
    document.body.style.color=config.style.dark.color
    var p = document.getElementsByTagName('p')
    var a = document.getElementsByTagName('a')
    var h1 = document.getElementsByTagName('h1')
    var h2 = document.getElementsByTagName('h2')
    var h3 = document.getElementsByTagName('h3')
    // for (var i = 0; i < h3.length; i++) {
    //   h3[i].style.color=config.style.dark.color
    // }
    // for (var i = 0; i < h2.length; i++) {
    //   h2[i].style.color=config.style.dark.color
    // }
    // for (var i = 0; i < h1.length; i++) {
    //   h1[i].style.color=config.style.dark.color
    // }
    // for (var i = 0; i < p.length; i++) {
    //   p[i].style.color=config.style.dark.color
    // }
    for (var i = 0; i < a.length; i++) {
      a[i].style.color=config.style.dark.color
    }
    document.body.style.background=config.style.dark.bgcolor
    document.getElementById('themebutton').innerHTML=config.lightbtntext
  } else if(config.theme=="light"){
    document.body.style.color=config.style.light.color
    var p = document.getElementsByTagName('p')
    var a = document.getElementsByTagName('a')
    for (var i = 0; i < p.length; i++) {
      p[i].style.color=config.style.light.color
    }
    for (var i = 0; i < a.length; i++) {
      a[i].style.color=config.style.light.color
    }
    document.body.style.background=config.style.light.bgcolor
    document.getElementById('themebutton').innerHTML=config.darkbtntext
  } else {
    config.theme="dark"
    setCookie("theme", "dark")
    theme()
  }
}
function menugen() {
  document.getElementById('Menus').innerHTML=""
  for (var i = 0; i < menus.length; i++) {
    document.getElementById('Menus').innerHTML=document.getElementById('Menus').innerHTML + "<br/><h2>" + menus[i].name + "</h2>"
    for (var x = 0; x < menus[i].buttons.length; x++) {
      menus[i].buttons[x]
      document.getElementById('Menus').innerHTML=document.getElementById('Menus').innerHTML + "<a href=" + `${menus[i].buttons[x].link}` + " id='btn1'><button class='menubutton' id='btn2'>" + `${menus[i].buttons[x].name}</button></a>  `
    }
  }
}
function bloggen() {
  var blogel=document.getElementById('Blog')
  blogel.innerHTML=`<h2>${blog.name}</h2>`
  for (var i = 0; i < blog.contents.length; i++) {
    if(blog.contents[i].text) {
      blogtext=blogel.innerHTML+"\n<!-- Text: " + blog.contents[i].text + " -->"
      if(blog.contents[i].marquee==true) {
        blogtext=blogtext+'<marquee behavior="alternate" truespeed="" scrolldelay="75">'
      }
      if (blog.contents[i].italic) {
        blogtext=blogtext+"<i>"
      }
      if (blog.contents[i].bold) {
        blogtext=blogtext+"<b style='font-weight:1000'>"
      }
      if (blog.contents[i].underlined) {
        blogtext=blogtext+"<u>"
      }
      blogtext=blogtext+blog.contents[i].text
      if (blog.contents[i].italic) {
        blogtext=blogtext+"</i>"
      }
      if (blog.contents[i].bold) {
        blogtext=blogtext+"</b>"
      }
      if (blog.contents[i].underlined) {
        blogtext=blogtext+"</u>"
      }
      if(blog.contents[i].marquee==true) {
        blogtext=blogtext+'</marquee>'
      }
      blogel.innerHTML=blogtext
    } else if (blog.contents[i].img) {
      blogtext=blogel.innerHTML+"\n"
      if(blog.contents[i].marquee==true) {
        blogtext=blogtext+'<marquee behavior="alternate" truespeed="" scrolldelay="66.75">'
      }
      blogtext=blogtext+"\n<!-- Image: " + blog.contents[i].img + " -->\n<br><img src=\"" + blog.contents[i].img + "\"></img>"
      if(blog.contents[i].marquee==true) {
        blogtext=blogtext+'</marquee>'
      }
      blogel.innerHTML=blogtext
    }
  }
}
function onload() {
  theme("yes")
  title()
  setupcolors()
  if(config.features.menus==true) {
    menugen()
  } else {
    document.getElementById('Menus').outerHTML=''
  }
  if(config.features.blog==true) {
    bloggen()
  }
  if(config.features.alpaca==true) {
    setInterval(function () {
      document.body.style.backgroundImage="url(https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2F4.bp.blogspot.com%2F-HHgZso8TpGM%2FUXN7mfbndXI%2FAAAAAAAACx0%2Fh1ZMIoB_Y0s%2Fs1600%2FAlpaca-Lovely-Animal.jpg&f=1&nofb=1)"
      document.body.style.filter="blur(0px)"
    }, 125);
  }
  if(config.features.selfdestruct==true) {
    document.getElementById('settings').innerHTML=""
    document.getElementById('settings').innerHTML=document.getElementById('settings').innerHTML + "/*"
    document.getElementById('settings').innerHTML=document.getElementById('settings').innerHTML + "\n  Settings hidden due to request"
    document.getElementById('settings').innerHTML=document.getElementById('settings').innerHTML + "\n  By TStudios"
    document.getElementById('settings').innerHTML=document.getElementById('settings').innerHTML + "\n"
    document.getElementById('settings').innerHTML=document.getElementById('settings').innerHTML + "\n  Copyright (c) 2017-2019 Biocloud Productions."
    document.getElementById('settings').innerHTML=document.getElementById('settings').innerHTML + "\n  All Rights Reserved."
    document.getElementById('settings').innerHTML=document.getElementById('settings').innerHTML + "\n\\*"
  }
  document.getElementById('noscript').outerHTML=""
  setTimeout(function () {
    try {
      document.getElementById('copyright').outerHTML=""
    } catch (e) {

    }
    document.body.outerHTML=document.body.outerHTML + "<small title='Copyright (c) 2017-2019 Biocloud Productions. All Rights Reserved.' id='copyright'><br><br>Copyright (c) 2017-2019 <a href='https://www.roblox.com/groups/5272592/Biocloud-Productions' title='Biocloud Productions is Copyright (c) 2017-2019 TStudios. All Rights Reserved.'>Biocloud Productions</a>. All Rights Reserved.<br>Generator/Builder by: TStudios</small> <!-- Removing the copyright line is Illegal. -->"
    // Removing/Modifying the line above is illegal
      theme()
  }, 625);
  for (var i = 0; i < document.getElementsByTagName('jsrequired').length; i++) {
    document.getElementsByTagName('jsrequired')[i].style.display="inherit"
  }
}
function togglethemes() {
  if(config.theme=="dark"){
    config.theme="light"
    setCookie("theme", config.theme)
  } else {
    config.theme="dark"
    setCookie("theme", config.theme)
  }
}
